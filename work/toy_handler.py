import json


def toy_handler(event, context):
    body = {
        "message": "Hello from authenticated End point !",
        "input": event
    }

    response = {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
        },
        "body": json.dumps(body)
    }

    return response