import json


def send_code(user_name, user_attributes, secret_code):
    print({
        "user_name": user_name,
        "user_attributes": user_attributes,
        "code": secret_code
    })
    # user_name is the phone number
    return


def get_secret_code():
    # we can use any complicated function here
    return "1234"


def lambda_handler(event, context):
    try:
        if (not event['request']['session'] or len(event['request']['session']) == 0 or
                (len(event['request']['session']) == 1 and
                    event['request']['session'][0]['challengeName'] == 'SRP_A')):
            # this is very first step, so we generate challenge and send to user
            print("New Secret Code")
            secret_code = get_secret_code()
        else:
            print("Old Secret Code")
            # we are here means user is still within his valid trials, we fetch the already generated challenge
            secret_code = event['request']['session'][-1]['challengeMetadata']

        if 'response' not in event:
            event['response'] = {}

        event['response']['publicChallengeParameters'] = {
            'userName': event['userName']
        }
        if ('privateChallengeParameters' not in event['response'] or
                event['response']['privateChallengeParameters'] is None):
            event['response']['privateChallengeParameters'] = {}

        # store the code for verification lambda
        event['response']['privateChallengeParameters']['secretLoginCode'] = secret_code
        # store the code for retry (same lambda next trial)
        event['response']['challengeMetadata'] = secret_code
        send_code(event['userName'], event['request']['userAttributes'], secret_code)
        print(event)
    except Exception as e:
        print(event)
        raise e
    return event
