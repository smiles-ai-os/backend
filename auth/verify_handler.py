def lambda_handler(event, context):
    try:
        if 'response' not in event:
            event['response'] = {}

        expected_answer = None
        try:
            expected_answer = event['request']['privateChallengeParameters']['secretLoginCode']
        except Exception as e:
            pass
        if event['request']['challengeAnswer'] == expected_answer:
            # matching the user provided answer with the expected answer
            # cognito, will provide the challengeAnswer
            event['response']['answerCorrect'] = True
            event['request']['userNotFound'] = False
        else:
            # if the challenge is not matched
            event['response']['answerCorrect'] = False
        print(event)
    except Exception as e:
        print(event)
        raise e
    return event
