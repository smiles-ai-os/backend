import os
import boto3


client = boto3.client('cognito-idp', region_name=os.environ['REGION'])
# this is a global singleton object (upto container)
NUMBER_OF_RETRY = 4


def create_user(user_pool_id, user_name):
    try:
        response = client.admin_create_user(
            UserPoolId=user_pool_id,
            Username=user_name
        )
        return "user_created"
    except Exception as e:
        return str(e)


def lambda_handler(event, context):
    print(event)
    try:
        if 'response' not in event:
            event['response'] = {}
        if (event['request']['session'] and
                len(event['request']['session']) >= NUMBER_OF_RETRY and
                not event['request']['session'][-1]['challengeResult']):
            print("Trial Exceeded")
            # 4 determines we give 4 retry attempts
            # this comes as error in frontend
            # frontend can detect and and give user option to retry
            event['response']['issueTokens'] = False
            event['response']['failAuthentication'] = True
        elif (event['request']['session'] and
                len(event['request']['session']) >= 1 and
                event['request']['session'][-1]['challengeResult']):
            print("auth done")
            # this is the case of correct authentication
            event['response']['issueTokens'] = True
            event['response']['failAuthentication'] = False
        else:
            print("OTP Trials Left")
            create_user(event['userPoolId'], event['userName'])  # this creaetes the user in cognito # auto signup
            event['response']['issueTokens'] = False
            event['response']['failAuthentication'] = False
            event['response']['challengeName'] = 'CUSTOM_CHALLENGE'
        print(event)
    except Exception as e:
        print(event)
        raise e
    return event
